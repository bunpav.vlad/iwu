<?php
/**
 * Created by PhpStorm.
 * User: pbunaev
 * Date: 13.09.18
 * Time: 15:59
 */
declare(strict_types=1);
namespace Depersonalizer;


class Parameters{
    private $original;
    private $target;
    private $configFile;

    private $config;
    public function __construct($original,$target,$configFile)
    {
        $this->original = $original;
        $this->target = $target;
        $this->configFile = $configFile;
    }

    public function issetFiles():bool
    {
        if (!file_exists($this->configFile)) {
            return false;
        }
        if (!file_exists($this->original)) {
            return false;
        }
        return true;
    }

    /**
     * @throws \Exception
     * @return $this
     */
    public function load(): Parameters
    {
        if (!$this->issetFiles()) {
            throw new \Exception('input files not found!');
        }
        $this->config = include $this->configFile;
        return $this;
    }

    public function getTables(): array
    {
        return array_keys($this->config);
    }

    public function getColumns($table): array
    {
        return $this->config[$table];
    }

    public function getTarget(): string
    {
        return $this->target;
    }

    public function getOriginal(): string
    {
        return $this->original;
    }
}