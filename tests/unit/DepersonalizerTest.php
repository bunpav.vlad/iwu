<?php
/**
 * Created by PhpStorm.
 * User: pbunaev
 * Date: 14.09.18
 * Time: 9:17
 */
declare(strict_types=1);
use Depersonalizer\Depersonalizer;
use Depersonalizer\Parameters;
use PHPUnit\Framework\TestCase;

class DepersonalizerTest extends TestCase
{
    const INSERT_LINE = 'INSERT INTO `user` (`email`, `role`, `companyPost`, `company`, `name`, `surname`, `phone`, `isEmailConfirmed`, `isAdmin`, `avatar`, `thumbnailPattern`, `encryptedPassword`, `isInvited`, `id`, `lastActivityAt`, `createdAt`, `updatedAt`, `isDeleted`)';
    const HEAD_VALUES_LINE = 'VALUES';
    const BODY_VALUES_LINE = "('4232456@mail.ru','employee','г. Москва',3,'Александр','Измайлов','79097503375',1,1,8,NULL,'$2a$10\$S//os/QbQjpWFCxTLZwapeUAYkjNBsI6YKI/CUWUjqKj/7U2g.Ddm',0,3,'2018-09-05 18:03:49','2017-04-18 11:10:16','2018-09-05 18:03:49',0),";
    const DEPERSONAL_VALUES_LINE = "\t('', 'employee', 'г. Москва', 3, '', '', '79097503375', 1, 1, 8, NULL, '$2a$10\$S//os/QbQjpWFCxTLZwapeUAYkjNBsI6YKI/CUWUjqKj/7U2g.Ddm', 0, 3, '2018-09-05 18:03:49', '2017-04-18 11:10:16', '2018-09-05 18:03:49', 0),";
    /**
     * @var Depersonalizer
     */
    private $depersonalizer;

    private $configFile;
    protected function setUp()
    {
        $this->configFile = __DIR__.'/../obfuscator.test.config.php';
        $this->depersonalizer = new Depersonalizer(
            (new Parameters(
                __DIR__.'/../test-input/users.sql.dump',
                __DIR__.'/../test-output/depersonal-users.sql.dump',
                $this->configFile)
            )->load()
        );
        parent::setUp();
    }


    public function testDepersonalizedLine()
    {
        $this->depersonalizer->defineMetaData($this::INSERT_LINE);
        $depersonal = $this->depersonalizer->depersonalizedLine($this::BODY_VALUES_LINE);
        $this->assertEquals($this::DEPERSONAL_VALUES_LINE,$depersonal,'Изменились настройки или сломался деперсонализатор');
    }

    public function testDefineMetaData()
    {
        $this->depersonalizer->defineMetaData($this::INSERT_LINE);
        $config = include $this->configFile;
        $tableName = key($config);
        $columnsToDepersonal = array_values(current($config));
        $closure=function(){
            return [
                'flag_values_header'  => $this->flag_values_header,
                'flag_values_process' => $this->flag_values_process,
                'flag_ignore_table'   => $this->flag_ignore_table,
                'find_columns'        => $this->find_columns,
                'use_table_name'      => $this->use_table_name
            ];
        };
        $inject = $closure->call($this->depersonalizer);
        $this->assertTrue($inject['flag_values_header'],'Не удалось определить INSERT инструкцию');
        $this->assertFalse($inject['flag_values_process'],'при определении INSERT не должно быть обработки VALUES');
        $this->assertTrue($inject['flag_ignore_table'],'Определив INSERT логика должна игнорировать остальные данные от таблице');
        $this->assertEquals($inject['use_table_name'],$tableName,'Не правильно выбралось имя таблицы');
        $this->assertArraySubset(array_values($inject['find_columns']),array_values($columnsToDepersonal), false, 'Найденныее колонки отличаются от настроенных');
    }

    public function testDefineValueMetaHeader()
    {
        $closure=function(){
            return $this->flag_values_process;
        };
        $this->depersonalizer->defineValueMetaHeader($this::HEAD_VALUES_LINE);
        $this->assertFalse($closure->call($this->depersonalizer),'INSERT не был определён, начинать процесс чтения VALUES рано');
        $this->depersonalizer->defineValueMetaHeader('some_string');
        $this->assertFalse($closure->call($this->depersonalizer),'чтение кастомной строки не должно было включить флаг обработки VALUES');
        $this->depersonalizer->defineValueMetaHeader($this::BODY_VALUES_LINE);
        $this->assertFalse($closure->call($this->depersonalizer),'без включения флагов строка с VALUES не должна начать разбираться');
        $this->depersonalizer->defineMetaData($this::INSERT_LINE);
        $this->depersonalizer->defineValueMetaHeader($this::HEAD_VALUES_LINE);
        $this->assertTrue($closure->call($this->depersonalizer),'На этом этапе должны были включить флаг обработки VALUES');
    }

    public function testDisposeFlags()
    {
        $closure=function(){
            return [
                'flag_values_header'  => $this->flag_values_header,
                'flag_values_process' => $this->flag_values_process,
                'flag_ignore_table'   => $this->flag_ignore_table,
                'find_columns'        => $this->find_columns,
            ];
        };
        $this->depersonalizer->defineMetaData($this::INSERT_LINE);
        $this->depersonalizer->defineValueMetaHeader($this::HEAD_VALUES_LINE);
        $this->depersonalizer->disposeFlags();
        $inject = $closure->call($this->depersonalizer);
        $this->assertFalse($inject['flag_values_header'],'ожидаем что сбросился флаг обработки VALUES');
        $this->assertFalse($inject['flag_values_process'],'ожидаем сброс флага процессинга VALUES');
        $this->assertFalse($inject['flag_ignore_table'],'ожидаем сброс флага для поиска новых INSERT');
        $this->assertTrue(empty($inject['find_columns']),'ожидаем что найденные колонки были сброшены');
    }
}
