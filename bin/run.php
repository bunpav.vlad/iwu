<?php
/**
 * Created by PhpStorm.
 * User: pbunaev
 * Date: 13.09.18
 * Time: 16:03
 */
declare(strict_types=1);
use Depersonalizer\Depersonalizer;
use Depersonalizer\Parameters;

require_once __DIR__.'/../vendor/autoload.php';

$input = getopt('o:t:c:',['original:','target::','config::']);
$parameters = new Parameters($input['o'] ?? $input['original'], $input['t'] ?? $input['target'],$input['c'] ?? $input['config']);
$parameters->load();

$depersonalizer = new Depersonalizer($parameters);
$depersonalizer->process();